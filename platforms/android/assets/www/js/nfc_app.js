var DEBUG = true;

var pchomeJSON;
var nfcTagID = 0;

function openWebPage(siteUrlIndex) {
    if (DEBUG) console.log("open => " + pchomeJSON.items[siteUrlIndex].url);
    var ref = window.open(encodeURI(pchomeJSON.items[siteUrlIndex].url), '_self', 'location=yes');
}

var nfc_app = {
    convertDec: function(number) {
        if (number < 0)
        {
            number = 0xFF + number + 1;
        }
        return number;
    },
    decToHexString: function(number) {
        return nfc_app.convertDec(number).toString(16).toUpperCase();
    },
    calculateId: function(tagId) {
        var number = nfc_app.convertDec(tagId[3]);
        number = number * 256 + nfc_app.convertDec(tagId[2]);
        number = number * 256 + nfc_app.convertDec(tagId[1]);
        number = number * 256 + nfc_app.convertDec(tagId[0]);
        return number;
    },
    readRemoteData: function() {
        $.ajax({
            type : "get",
            contentType : 'application/json; charset=utf-8',
            crossDomain : true,
            cache : false,
            dataType : 'json',
            async : false,
            timeout : 2000,
            url : 'http://192.168.1.67/login.php?tag_id='+nfcTagID,
            data : '{}',
            success : function(msg) {
                if (DEBUG) console.log("readRemoteData success");
                pchomeJSON = msg;
            },
            error : function(msg) {
                if (DEBUG) console.log("readRemoteData error");
                pchomeJSON = {};
            }
        });
    },
    showShopping: function() {
        var newline = "";
        $("#shopping").html("");
        nfc_app.readRemoteData();
        newline = "<li data-role=\"list-divider\">User information</li>";
        newline += "<li><h2>User ID</h2><p>" + pchomeJSON.user.id + "</p></li>";
        newline += "<li data-role=\"list-divider\">Suggestion items</li>";
        $("#shopping").append(newline);
        for (var i = 0; i < pchomeJSON.items.length; i++) {
            newline = "<li>";
            newline += "<a href=\"#\" onclick=\"openWebPage('"+i+"')\">";
            newline += "<img src=\"" + pchomeJSON.items[i].img +"\">";
            newline += "<h2>" + pchomeJSON.items[i].desc + "</h2>";
            newline += "</a>";
            newline += "</li>";
            if (DEBUG) console.log("newline = " + newline);
            $("#shopping").append(newline);
        }
        $("#shopping").listview("refresh");
    },
    nfc1FunctionInit: function() {
        if (DEBUG) console.log("nfc1FunctionInit is called");
        $("#popupDialog").popup({
            transition: "pop",
            afterclose: function(event, ui) {
                nfc_app.showShopping();
            }
        });
        $("#popupWaiting").popup({
            transition: "pop",
            afterclose: function(event, ui) {
            }
        });
        $("#popupWarning").popup({
            transition: "pop",
            afterclose: function(event, ui) {
                navigator.app.exitApp();
            }
        });
        nfc.addTagDiscoveredListener (
            function (nfcEvent) {
                var tag = nfcEvent.tag, id = tag.id, techTypes = tag.techTypes;
                console.log('Tag is received');
                if (DEBUG) console.log("Tag is received");
                if (DEBUG) console.log(nfc_app.calculateId(id) + "(" + nfc_app.decToHexString(id[0]) + nfc_app.decToHexString(id[1]) + nfc_app.decToHexString(id[2]) + nfc_app.decToHexString(id[3]) + ")");
                $("#tag_id_display").text("ID: " + nfc_app.calculateId(id));
                nfcTagID = nfc_app.calculateId(id);
                $("#popupWaiting").popup("close");
                $("#popupDialog").popup("open");
            },
            function () {
                if (DEBUG) console.log("Waiting for tag");
                $("#popupWaiting").popup("open");
            },
            function (error) {
                if (DEBUG) console.log("Error adding tag listener " + JSON.stringify(error));
            }
        );
    }
};